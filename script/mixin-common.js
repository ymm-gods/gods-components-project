/**
 * JWT及公共功能，便于复用代码
 */
export default {
    data () {
        return {
            // TOKEN前缀
            tokenPrefix: '',
            // TOKEN后缀
            tokenSuffix: '',
            // AccessToken名
            storeAccessTokenName: this._storeAccessTokenName || 'access_token',
            // RefreshToken名
            storeRefreshTokenName: this._storeRefreshTokenName || 'refresh_token',
        }
    },
    methods: {
        /**
         * 安全链接处理
         * @param params
         * @param link
         * @returns {boolean}
         */
        safeLinkFunc (params, link = window.location.href) {
            let safeLink = link
            if (Object.prototype.toString.call(params) === '[object String]') {
                safeLink = safeLink.replace(new RegExp(`[?|&]${params}=[%+-_0-9a-zA-z]*`), '')
            } else if (Object.prototype.toString.call(params) === '[object Array]') {
                for (let i = 0, length = params.length; i < length; i++) {
                    safeLink = safeLink.replace(new RegExp(`[?|&]${params[i]}=[%+-_0-9a-zA-z]*`), '')
                }
            } else if (Object.prototype.toString.call(params) === '[object Object]') {
                for (let key in params) {
                    safeLink = safeLink.replace(new RegExp(`[?|&]${key}=[%+-_0-9a-zA-z]*`), params[key])
                }
            }
            if (link !== safeLink) {
                // 清除历史记录，避免回退
                window.history.forward(1)
                window.location.href = safeLink
                return false
            }
            return true
        },
        /**
         * 获取JWT荷载
         * @param jwt
         * @returns {{user_name}|*|boolean}
         */
        getJwtPayloadFunc (jwt) {
            try {
                let jwtArray = jwt.split('.')
                let jwtPayload = jwtArray[1]
                // eslint-disable-next-line no-undef
                jwtPayload = JSON.parse(this.decodeBase64Func(jwtPayload))
                if (jwtPayload.user_name) {
                    // eslint-disable-next-line no-undef
                    jwtPayload.user_name = this.decodeBase64Func(jwtPayload.user_name)
                }
                return jwtPayload
            } catch (exception) {
                console.log('[getJwtPayloadFunc]#异常', exception)
                return false
            }
        },
        // Base64编码
        encodeBase64Func (string) {
            // eslint-disable-next-line no-undef
            return $GP.Util.cryptoJS.enc.Base64.stringify($GP.Util.cryptoJS.enc.Utf8.parse(string))
        },
        // Base64解码
        decodeBase64Func (string) {
            // eslint-disable-next-line no-undef
            return $GP.Util.cryptoJS.enc.Base64.parse(decodeURIComponent(string)).toString($GP.Util.cryptoJS.enc.Utf8)
        },
        // region 认证信息处理方法 +++++
        /**
         * 设置TOKEN方法
         * @param accessToken
         * @param refreshToken
         * @returns {boolean}
         */
        setTokenFunc (accessToken = '', refreshToken = '') {
            try {
                this.setDataHubTokenFunc(accessToken, refreshToken)
                this.setLocalTokenFunc(accessToken, refreshToken)
                return this.getTokenFunc()
            } catch (exception) {
                console.log('[setTokenFunc]#异常', exception)
                return false
            }
        },
        /**
         * 获取TOKEN方法
         * @returns {{accessToken: *, refreshToken: *}|boolean|{accessToken: string, refreshToken: string}}
         */
        getTokenFunc () {
            try {
                return this.getDataHubTokenFunc() || this.getLocalTokenFunc()
            } catch (exception) {
                console.log('[getTokenFunc]#异常', exception)
                return false
            }
        },
        /**
         * 删除TOKEN方法
         * @returns {boolean}
         */
        delTokenFunc () {
            try {
                this.delDataHubTokenFunc()
                this.delLocalTokenFunc()
                return true
            } catch (exception) {
                console.log('[delTokenFunc]#异常', exception)
                return false
            }
        },
        /**
         * 设置数据总线上的TOKEN
         * @param accessToken
         * @param refreshToken
         * @returns {boolean}
         */
        setDataHubTokenFunc (accessToken = '', refreshToken = '') {
            try {
                if (accessToken) {
                    let accessTokenName = this.getAccessTokenNameFunc()
                    accessTokenName && this.dataHubSet(accessTokenName, accessToken)
                }
                if (refreshToken) {
                    let refreshTokenName = this.getRefreshTokenNameFunc()
                    refreshTokenName && this.dataHubSet(refreshTokenName, refreshToken)
                }
                return true
            } catch (exception) {
                console.log('[setDataHubTokenFunc]#异常', exception)
                return false
            }
        },
        /**
         * 从数据总线获取TOKEN
         * @returns {{accessToken: *, refreshToken: *}|boolean}
         */
        getDataHubTokenFunc () {
            try {
                let accessTokenName = this.getAccessTokenNameFunc()
                let accessToken = accessTokenName && this.dataHubGet(accessTokenName, '')
                let refreshTokenName = this.getRefreshTokenNameFunc()
                let refreshToken = refreshTokenName && this.dataHubGet(refreshTokenName, '')
                if (accessToken && refreshToken) {
                    return {
                        accessToken,
                        refreshToken
                    }
                }
                return false
            } catch (exception) {
                console.log('[getDataHubTokenFunc]#异常', exception)
                return false
            }
        },
        /**
         * 从数据总线删除TOKEN
         * @returns {boolean}
         */
        delDataHubTokenFunc () {
            try {
                let accessTokenName = this.getAccessTokenNameFunc()
                accessTokenName && this.dataHubSet(accessTokenName, '')
                let refreshTokenName = this.getRefreshTokenNameFunc()
                refreshTokenName && this.dataHubSet(refreshTokenName, '')
                return true
            } catch (exception) {
                console.log('[getDataHubTokenFunc]#异常', exception)
                return false
            }
        },
        /**
         * 设置本地存储上的TOKEN
         * @param accessToken
         * @param refreshToken
         * @returns {boolean}
         */
        setLocalTokenFunc (accessToken = '', refreshToken = '') {
            try {
                if (accessToken) {
                    let accessTokenName = this.getAccessTokenNameFunc()
                    accessTokenName && window.localStorage.setItem(accessTokenName, accessToken)
                }
                if (refreshToken) {
                    let refreshTokenName = this.getRefreshTokenNameFunc()
                    refreshTokenName && window.localStorage.setItem(refreshTokenName, refreshToken)
                }
                return true
            } catch (exception) {
                console.log('[setLocalTokenFunc]#异常', exception)
                return false
            }
        },
        /**
         * 从本地存储获取TOKEN
         * @returns {{accessToken: string, refreshToken: string}|boolean}
         */
        getLocalTokenFunc () {
            try {
                let accessTokenName = this.getAccessTokenNameFunc()
                let accessToken = accessTokenName && window.localStorage.getItem(accessTokenName)
                let refreshTokenName = this.getRefreshTokenNameFunc()
                let refreshToken = refreshTokenName && window.localStorage.getItem(refreshTokenName)
                if (accessToken && refreshToken) {
                    return {
                        accessToken,
                        refreshToken
                    }
                }
                return false
            } catch (exception) {
                console.log('[getLocalTokenFunc]#异常', exception)
                return false
            }
        },
        /**
         * 从本地存储删除TOKEN
         * @returns {boolean}
         */
        delLocalTokenFunc () {
            try {
                let accessTokenName = this.getAccessTokenNameFunc()
                accessTokenName && window.localStorage.removeItem(accessTokenName)
                let refreshTokenName = this.getRefreshTokenNameFunc()
                refreshTokenName && window.localStorage.removeItem(refreshTokenName)
                return true
            } catch (exception) {
                console.log('[getLocalTokenFunc]#异常', exception)
                return false
            }
        },
        /**
         * 获取AccessToken名称
         * @returns {string}
         */
        getAccessTokenNameFunc () {
            return this.tokenPrefix + this.storeAccessTokenName + this.tokenSuffix
        },
        /**
         * 获取RefreshToken名称
         * @returns {string}
         */
        getRefreshTokenNameFunc () {
            return this.tokenPrefix + this.storeRefreshTokenName + this.tokenSuffix
        },
        // endregion
    }
}
