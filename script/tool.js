/**
 * 工具库
 */
function loadJs (url = '') {
    if (!url) return false
    if (!loadJs.cache) loadJs.cache = {}
    if (loadJs.cache[url]) return Promise.resolve(true)
    return new Promise((resolve, reject) => {
        let target = document.createElement('script')
        target.src = url
        target.async = true
        target.onload = () => {
            loadJs.cache[url] = 'cached'
            resolve(true)
        }
        target.onerror = () => {
            console.error(`加载失败:${url}`)
            resolve(false)
        }
        document.head.appendChild(target)
    })
}

function loadCss (url = '') {
    if (!url) return false
    if (!loadCss.cache) loadCss.cache = {}
    if (loadCss.cache[url]) return Promise.resolve(true)
    return new Promise((resolve, reject) => {
        let target = document.createElement('link')
        target.href = url
        target.rel = 'stylesheet'
        target.type = 'text/css'
        target.async = true
        target.onload = () => {
            loadCss.cache[url] = 'cached'
            resolve(true)
        }
        target.onerror = () => {
            console.error(`加载失败:${url}`)
            resolve(false)
        }
        document.head.appendChild(target)
    })
}

export default {
    loadJs,
    loadCss
}